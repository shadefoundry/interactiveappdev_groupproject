﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace CardGame
{
    public sealed partial class CardOptionsDialog : ContentDialog
    {

        public CardOptionsDialog(Card card)
        {

            this.InitializeComponent();
            // init fields
            Card = card;
            CardName = card.CardName;
            ImagePath = card.ImagePath;

            // set all XAML controls
            _lblTitle.Text = CardName;
            _imgCardImage.Source = new BitmapImage(new Uri("ms-appx:///" + ImagePath));

            CardColor = card.CardColor;
            CardFile = card.CardImageName;
            CardCost = card.CardCost;
            CardAmount = card.CardAmount;

        }

        #region properties
        private string CardName { get; set; }
        private string ImagePath { get; set; }
        private string CardColor { get; set; }
        private string CardFile { get; set; }
        private string CardCost { get; set; }
        private int CardAmount { get; set; }
        private Card Card { get; set; }

        #endregion

        #region event handlers
        /// <summary>
        /// hides the dialog when the pointer exits the dialog
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPointerExited(PointerRoutedEventArgs e)
        {
            Hide();
        }

        private async void OnAddCardToMyCards(object sender, RoutedEventArgs e)
        {

            // change IsMyCard to true
            Card.IsMyCard = true;
            Card.CardAmount = 1;

            //determine a default root folder where the application is allowed to write
            Library.RootDirectory = ApplicationData.Current.LocalFolder.Path;

            string dataDirPath = Path.Combine(Library.RootDirectory, "cards");

            //update the path to match the path for account text files
            dataDirPath = Path.Combine(dataDirPath, "JSON");

            //determine the account file name
            string cardFileName = String.Format("card{0}.json", Card.CardName);

            string cardFilePath = Path.Combine(dataDirPath, cardFileName);


            //write the account data to the file
            using (FileStream cardStream = new FileStream(cardFilePath, FileMode.Create))
            {
                //serialize the account object
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Card));

                serializer.WriteObject(cardStream, Card);
            }

            // show dialog message

            var messageDialog = new MessageDialog(string.Format("{0} has been added successfully.", CardName));
            await messageDialog.ShowAsync();

            // exit the dialog
            Hide();

        }

        #endregion

    }
}
