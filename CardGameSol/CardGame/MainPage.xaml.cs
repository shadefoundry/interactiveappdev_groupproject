﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace CardGame
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // display the backbutton on the frame
            // use static method from system navigation manager
            SystemNavigationManager navMgr = SystemNavigationManager.GetForCurrentView();
            navMgr.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            navMgr.BackRequested += OnBackRequested;
            // setup event handler for back button could do it here in the application or globally
            base.OnNavigatedTo(e);

        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
                e.Handled = true; // system arrow back
            }
        }

        private void OnShowMenuPressed(object sender, RoutedEventArgs e)
        {
            // we want the pane to close it it is open and open when it is closed
            _splitViewMenu.IsPaneOpen = !_splitViewMenu.IsPaneOpen; 
        }

        private void OnMenuLibraryPressed(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Library));
        }

        private void OnMenuStatsPressed(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MyCards));
        }
    }
}
