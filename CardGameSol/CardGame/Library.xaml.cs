﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace CardGame
{
    /// <summary>
    /// Library class. TODO: discribtion
    /// Author: Kevin
    /// </summary>
    public sealed partial class Library : Page
    {

        //load all attributes of cards by seperate text files
        string[] _cardsArray = System.IO.File.ReadAllLines(@"Cards\\Data\\CardNames.txt");
        string[] _cardsColors = System.IO.File.ReadAllLines(@"Cards\\Data\\CardColors.txt");
        string[] _cardsCosts = System.IO.File.ReadAllLines(@"Cards\\Data\\CardCosts.txt");
        string[] _cardImages = System.IO.File.ReadAllLines(@"Cards\\Data\\CardImages.txt");
        //public static List<string> myCardsList = new List<string>();



        //define list of cards
        public static List<Card> s_lstDatabase = new List<Card>();
        public static List<Card> s_cardList = new List<Card>();
        public Library()
        {
            this.InitializeComponent();
            //createDatabase(); // needed when we want recreate the basic json files.
            Load();

            ResetDatabaseDisplay();

            _lstAllCardsList.SelectionChanged += OnCardSelected;
        }
        #region properties
        public static string RootDirectory { get; set; }
        #endregion

        #region methods

        private void AddCardToView(Card card)
        {
            _lstAllCardsList.Items.Add(card);
        }

        /// <summary>
        /// to create a list of all cards from txt files
        /// needed when we want recreate the basic json files.
        /// </summary>
        private void CreateDatabase()
        {

            ////determine a default root folder where the application is allowed to write
            //RootDirectory = ApplicationData.Current.LocalFolder.Path;

            ////create a list of cards, each with a name, color, cost and image file
            //_lstDatabase.Clear();
            //for (int i = 0; i < cardsArray.Length; i++)
            //{
            //    Card card = new Card(cardsArray[i], cardImages[i], cardsColors[i], cardsCosts[i]);
            //    _lstDatabase.Add(card);


            //}


            //    string dataDirPath = Path.Combine(RootDirectory, "cards");
            //    //update the path to match the path for account text files
            //    dataDirPath = Path.Combine(dataDirPath, "JSON");

            //    //make the directory if it does not exist
            //    if (Directory.Exists(dataDirPath) == false) // The ! operator could be used but it is not very visible
            //    {
            //        Directory.CreateDirectory(dataDirPath);
            //    }


            //    // write json file
            //    //go through each card in the list of accounts and ask it to save itself into a corresponding file
            //    foreach (Card cardOb in _lstDatabase)
            //    {
            //        //determine the account file name
            //        string cardFileName = String.Format("card{0}.json", cardOb.CardName);

            //        string cardFilePath = Path.Combine(dataDirPath, cardFileName);

            //        //write the account data to the file
            //        using (FileStream acctStream = new FileStream(cardFilePath, FileMode.Create))
            //        {
            //            //serialize the account object
            //            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Card));

            //            serializer.WriteObject(acctStream, cardOb);
            //        }

            //    }
            //    //TODO commenting ended here
        }

        public void Load()
        {
            RootDirectory = ApplicationData.Current.LocalFolder.Path;

            string dataDirPath = Path.Combine(RootDirectory, "cards");

            //update the path to match the path for account text files
            dataDirPath = Path.Combine(dataDirPath, "JSON");

            //get the list of files in the directory
            string[] cardFileList = Directory.GetFiles(dataDirPath);

            //go through the list of files, create the appropriate accounts and load the file
            foreach (string cardFileName in cardFileList)
            {
                using (FileStream cardStream = new FileStream(cardFileName, FileMode.Open))
                {

                    //deserialize the account object
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Card));
                    Card card = serializer.ReadObject(cardStream) as Card;

                    if (card.IsMyCard == false)
                    {
                        //add the card to the list of accounts
                        s_cardList.Add(card);

                    }
                }
            }
        }



        //resets _lstAllCardsList to the way it was initially.
        private void ResetDatabaseDisplay()
        {
            _lstAllCardsList.Items.Clear();

            foreach (Card card in s_cardList)
            {
                string cardName = card.CardName;
                string cardColor = card.CardColor;
                _lstAllCardsList.Items.Add(card);
            }

        }


        #endregion



        #region event handlers

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // display the backbutton on the frame
            // use static method from system navigation manager
            SystemNavigationManager navMgr = SystemNavigationManager.GetForCurrentView();
            navMgr.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            navMgr.BackRequested += OnBackRequested;
            // setup event handler for back button could do it here in the application or globally
            base.OnNavigatedTo(e);

        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
                e.Handled = true; // system arrow back
            }
        }

        //bring up options for cards
        private async void OnCardSelected(object sender, SelectionChangedEventArgs e)
        {

            foreach(Card card in s_cardList)
            {
                
                if(_lstAllCardsList.SelectedItem as Card == card)
                {
                    // the selected card found in the list

                    // show card options dialog
                    // send all the information about the card in case the user wants to save the card.
                    CardOptionsDialog dialog = new CardOptionsDialog(card);
                    await dialog.ShowAsync();
                }
            }

        }

        private void OnMenuLibraryPressed(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Library));
        }

        private void OnShowMenuPressed(object sender, RoutedEventArgs e)
        {
            // we want the pane to close it it is open and open when it is closed
            _splitViewMenu.IsPaneOpen = !_splitViewMenu.IsPaneOpen;
        }

        private void OnMenuMyCardsPressed(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MyCards));
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void OnSearchText(object sender, RoutedEventArgs e)
        {
            //clear the list box
            _lstAllCardsList.Items.Clear();

            //clear comboboxes since we're searching for card names
            _comboCardColor.SelectedIndex = -1;
            _comboCardCost.SelectedIndex = -1;

            //define a string that's equal to the _txtCardNameBox text
            string searchText = _txtCardNameBox.Text;

            //if the searchtext isnt empty
            if (searchText != "")
            {

                //go through all the cards and show only values matching search parameters
                foreach (Card card in s_cardList)
                {
                    //if the card name contains the search text

                    if (card.CardName.Contains(searchText))
                    {
                        //display the card name
                        AddCardToView(card);
                    }
                }

            }
            //if the search text is empty just say fuck it and display everything
            else {
                ResetDatabaseDisplay();
            }
        }


        private void OnSearchColor(object sender, RoutedEventArgs e)
        {
            //clear the list box
            _lstAllCardsList.Items.Clear();

            //clear search text and card cost since we're searching for colors
            _txtCardNameBox.Text="";
            _comboCardCost.SelectedIndex = -1;


            string searchColor = _comboCardColor.SelectedItem.ToString();

            //go through all the cards and show only values matching search parameters
            foreach (Card card in s_cardList)
            {

                //if the card name contains the search text
                if (card.CardColor.Contains(searchColor))
                {
                    //display the card name
                    AddCardToView(card);
                }

            }

        }

        private void OnSearchCost(object sender, RoutedEventArgs e)
        {
            //clear the list box
            _lstAllCardsList.Items.Clear();
            //clear search text and card color since we're searching for costs
            _txtCardNameBox.Text = "";


            _comboCardColor.SelectedIndex = -1;
            //go through all the cards and show only values matching search parameters

            string searchCost = _comboCardCost.SelectedItem.ToString();




            //go through all the cards and show only values matching search parameters
            foreach (Card card in s_cardList)
            {
                //if the card name contains the search text
                if (card.CardCost.Contains(searchCost))
                {
                    //display the card name
                    AddCardToView(card);
                }
            }
        }

        private void ComboColorInit(FrameworkElement sender, object args)
        {
            _comboCardColor.Items.Add("White");
            _comboCardColor.Items.Add("Blue");
            _comboCardColor.Items.Add("Black");
            _comboCardColor.Items.Add("Red");
            _comboCardColor.Items.Add("Green");
            _comboCardColor.Items.Add("Artifact");
            _comboCardColor.Items.Add("Land");
        }

        private void ComboCostInit(FrameworkElement sender, object args)
        {
            for (int i = 0; i < 10; i++) {
                _comboCardCost.Items.Add(i);
            }
        }
        //TODO commenting starts here
        //creates the card database. This is only called once.


        private void OnLoading(FrameworkElement sender, object args)
        {

        }

        #endregion

    }

}
