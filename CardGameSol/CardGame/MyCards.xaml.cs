﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace CardGame
{
    /// <summary>
    /// My Cards page, created to show the user's list of cards.
    /// and let the user take specific actions
    /// e.g. delete a card and navidate between pages.
    /// Author: Hatim Hoho
    /// </summary>
    public sealed partial class MyCards : Page
    {
        // list of cards. strings as test
        public static HashSet<Card> s_myCardsList = new HashSet<Card>();

        //public static List<Card> _myCardObjects = new List<Card>();
        //public static List<string> cardNamesList = new List<string>();

        public MyCards()
        {

            this.InitializeComponent();

            //getAllCards();
            LoadCards();
            _lstBoxMyCards.SelectionChanged += OnCardSelected;
        }

        #region properties
        public string CardName { get; set; }
        public string RootDirectory { get; set; }

        #endregion


        #region methods

        /// <summary>
        /// emplements all cards names into the listbox
        /// </summary>
        private void getAllCards()
        {

            _lstBoxMyCards.ItemsSource = s_myCardsList;


        }
        public void LoadCards()
        {
            RootDirectory = ApplicationData.Current.LocalFolder.Path;

            string dataDirPath = Path.Combine(RootDirectory, "cards");

            //update the path to match the path for account text files
            dataDirPath = Path.Combine(dataDirPath, "JSON");

            //get the list of files in the directory
            string[] cardFileList = Directory.GetFiles(dataDirPath);

            //go through the list of files, create the appropriate accounts and load the file
            foreach (string cardFileName in cardFileList)
            {
                using (FileStream cardStream = new FileStream(cardFileName, FileMode.Open))
                {

                    //deserialize the account object
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Card));
                    Card card = serializer.ReadObject(cardStream) as Card;

                    // if the IsMyCard is true -> ad it to the ListView
                    if (card.IsMyCard)
                    {
                        _lstBoxMyCards.Items.Add(card);
                        s_myCardsList.Add(card);
                    }
                }
            }
        }



        #endregion

        #region event handlers


        /// <summary>
        /// Suzanne, Hatim - Event handler, called when the card is selected.
        /// find the right card, and pass it to a dialog page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnCardSelected(object sender, SelectionChangedEventArgs e)
        {
            foreach (Card card in s_myCardsList)
            {

                if (_lstBoxMyCards.SelectedItem as Card == card)
                {
                    // the selected card found in the list

                    // show card options dialog
                    // send all the information about the card in case the user wants to save the card.
                    ShowCard dialog = new ShowCard(card);
                    await dialog.ShowAsync();

                    break;
                }
            }


        }

        /// <summary>
        /// Suzanne - OnShowMenuPressed expands the split view. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnShowMenuPressed(object sender, RoutedEventArgs e)
        {
            // we want the pane to close it it is open and open when it is closed
            _splitViewMenu.IsPaneOpen = !_splitViewMenu.IsPaneOpen;
        }

        /// <summary>
        /// Suzanne - Navigate to the library page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMenuLibraryPressed(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Library)); // navigate to library
        }

        /// <summary>
        /// refrish the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRefreshPressed(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MyCards)); // navigate to library

        }
        #endregion

    }
}
