﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CardGame
{
    /// <summary>
    /// Card class. It stores every cards information and methods.
    /// </summary>

    [DataContract]
    public class Card
    {
        
        public Card(string cardName, string cardImageName, string cardColor, string cardCost) {

            // init properties
            CardName = cardName;
            CardImageName = cardImageName;
            ImagePath = @"Cards/" + CardImageName;
            
            CardColor = cardColor;
            CardCost = cardCost;
            IsMyCard = false;
            CardAmount = 0;
        }

        #region properties

        [DataMember(Name = "Name")]
        public string CardName { get; set;}

        [DataMember(Name = "ImgName")]
        public string CardImageName { get; set; }

        [DataMember(Name = "ImgPath")]
        public string ImagePath { get; set; }

        [DataMember(Name = "Color")]
        public string CardColor { get; set; }

        [DataMember(Name = "Price")]
        public string CardCost { get; set; }

        [DataMember(Name = "Amount")]
        public int CardAmount { get; set; }

        [DataMember(Name = "IsMyCard")]
        public bool IsMyCard { get; set; }


        #endregion

        #region methods

        public void CutOneFromTheAmount()
        {
            CardAmount -= 1;
        }
        public void AddOneFromTheAmount()
        {
            CardAmount += 1;
        }

        #endregion
    }
}
