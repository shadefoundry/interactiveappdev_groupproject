﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace CardGame
{
    /// <summary>
    /// Author: Suzanne
    /// Description: A custom Dialog page that displays the card and options to the user.
    /// </summary>
    public sealed partial class ShowCard : ContentDialog
    {

        public ShowCard(Card card)
        {
            this.InitializeComponent();

            Card = card;
            CardName = card.CardName;
            ImagePath = card.ImagePath;
            CardAmount = card.CardAmount;

            // set all XAML controls
            _lblTitle.Text = card.CardName;
            _imgCardImage.Source = new BitmapImage(new Uri("ms-appx:///" + @"Cards/" + card.CardImageName));

            // Suzanne - set the label to display the number of copies the user has
            _lblCardAmount.Text = string.Format("{0} out of 4", card.CardAmount);
            
        }

        #region properties
        private string CardName { get; set; }
        private string ImagePath { get; set; }
        private int CardAmount { get; set; }
        private Card Card { get; set; }

        #endregion

        #region event handlers
        protected override void OnPointerExited(PointerRoutedEventArgs e)
        {
            (Window.Current.Content as Frame)?.Navigate(typeof(MyCards)); // navigate to library

            Hide();
        }


        private async void OnDeleteCard(object sender, RoutedEventArgs e)
        {

            // delete the card object
            try
            {
                // change IsMyCard to false
                Card.IsMyCard = false;

                //determine a default root folder where the application is allowed to write
                Library.RootDirectory = ApplicationData.Current.LocalFolder.Path;

                string dataDirPath = Path.Combine(Library.RootDirectory, "cards");
                //update the path to match the path for account text files
                dataDirPath = Path.Combine(dataDirPath, "JSON");

                //determine the account file name
                string cardFileName = String.Format("card{0}.json", Card.CardName);

                string cardFilePath = Path.Combine(dataDirPath, cardFileName);


                //write the account data to the file
                using (FileStream cardStream = new FileStream(cardFilePath, FileMode.Create))
                {
                    //serialize the account object
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Card));

                    serializer.WriteObject(cardStream, Card);
                }

                // show dialog message

                var messageDialog = new MessageDialog(string.Format("{0} has been deleted successfully.", CardName));
                await messageDialog.ShowAsync();

                // exit the dialog
                Hide();
            }
            catch
            {
                // show dialog message
                var messageDialog = new MessageDialog("Something went wrong.");
                await messageDialog.ShowAsync();

            }
            finally
            {
                // exit the dialog
                Hide();

            }
        }


        private async void OnAddCard(object sender, RoutedEventArgs e)
        {

            if (Card.CardAmount < 4)
            {
                // delete the card object
                try
                {
                    // change IsMyCard to false
                    Card.CardAmount += 1;

                    //determine a default root folder where the application is allowed to write
                    Library.RootDirectory = ApplicationData.Current.LocalFolder.Path;

                    string dataDirPath = Path.Combine(Library.RootDirectory, "cards");
                    //update the path to match the path for account text files
                    dataDirPath = Path.Combine(dataDirPath, "JSON");

                    //determine the account file name
                    string cardFileName = String.Format("card{0}.json", Card.CardName);

                    string cardFilePath = Path.Combine(dataDirPath, cardFileName);


                    //write the account data to the file
                    using (FileStream cardStream = new FileStream(cardFilePath, FileMode.Create))
                    {
                        //serialize the account object
                        DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Card));

                        serializer.WriteObject(cardStream, Card);

                        // Suzanne - set the label to display the number of copies the user has
                        _lblCardAmount.Text = string.Format("{0} out of 4", Card.CardAmount);

                    }


                }
                catch
                {
                    // show dialog message
                    var messageDialog = new MessageDialog("Something went wrong.");
                    await messageDialog.ShowAsync();

                }
            }

        }


        private async void OnCutCard(object sender, RoutedEventArgs e)
        {
            if (Card.CardAmount > 1)
            {
                // delete the card object
                try
                {
                    // change IsMyCard to false
                    Card.CardAmount -= 1;

                    //determine a default root folder where the application is allowed to write
                    Library.RootDirectory = ApplicationData.Current.LocalFolder.Path;

                    string dataDirPath = Path.Combine(Library.RootDirectory, "cards");
                    //update the path to match the path for account text files
                    dataDirPath = Path.Combine(dataDirPath, "JSON");

                    //determine the account file name
                    string cardFileName = String.Format("card{0}.json", Card.CardName);

                    string cardFilePath = Path.Combine(dataDirPath, cardFileName);


                    //write the account data to the file
                    using (FileStream cardStream = new FileStream(cardFilePath, FileMode.Create))
                    {
                        //serialize the account object
                        DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Card));

                        serializer.WriteObject(cardStream, Card);

                        // Suzanne - set the label to display the number of copies the user has
                        _lblCardAmount.Text = string.Format("{0} out of 4", Card.CardAmount);

                    }


                }
                catch
                {
                    // show dialog message
                    var messageDialog = new MessageDialog("Something went wrong.");
                    await messageDialog.ShowAsync();

                }
            }

        }
        #endregion
    }
}
