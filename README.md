# README #

This is a simple card viewer for the collectible card game "Magic: the Gathering" built on the Universal Windows Platform
It features
* card search 
* card browsing by name, type, color, etc.
* Simple collection management
Currently it only supports one set which is hard-coded in due to time constraints of the project.